/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eftal.prog2;

/**
 *
 * @author artinsystems
 */
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class Client {

    private String baseURL = "https://newsapi.org/v2/";
    private String apiKey = "584948e25ec44ef3bda12796227b6bf6";
    private String searchKey = "";
    private HttpClient httpClient;

    public Client() {
        httpClient = HttpClientBuilder.create().build();
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public String getBaseURL() {
        return this.baseURL;
    }

    public String getSearchKey() {
        return this.searchKey;
    }

    public String getNews() throws IOException {
        String prepareURL = getBaseURL() + "everything?q=" + getSearchKey() + "&apiKey=" + getApiKey();
        HttpGet request = new HttpGet(prepareURL);
        try (CloseableHttpResponse response = (CloseableHttpResponse) httpClient.execute(request)) {

            // Get HttpResponse Status
            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();
            String result = EntityUtils.toString(entity);
            System.out.println(result);

            return result;

        }
    }

}
